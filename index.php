<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Belajar PHP Function, Conditional, dan Looping</h1>

    <?php
        $ternary = true;
        $ternary ? print("Ternary bernilai TRUE") : print("Ternary bernilai FALSE");
        echo "<br>";

        if($ternary) {
            print("Ternary bernilai TRUE");
        } else {
            print("Ternary bernilai FALSE");
        }
    
        echo "<br>";

        function perkenalan ($nama) {
            return "Perkenalkan, Nama Saya" . $nama . "<br>";
        }

        $hasildari=perkenalan(" farel");
        echo perkenalan(" budi");
        echo $hasildari;

        $arr=array("kamu1","kamu2","kamu3");
        for($i=0;$i < count($arr);$i++){
            echo $arr[$i] . "<br>";
        }
        
        foreach($arr as $value){
            echo $value . "<br>";
        }


        /*
        perkenalan(" farel");
        Perkenalan(" budi");
        */
        echo "<br>";


    
    
    
    ?>    

</body>
</html>